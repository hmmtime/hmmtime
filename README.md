# Hmmtime

Hmmtime is an application to help show and compare the different timezones around the world. Hmm is short for Hummingbird, the mascot of Hmmtime. Hummingbirds seem to remember where and when they visited individual flowers for nectar. Birds are not limited by boundaries and timezones. By providing this tool as free open source we try to remove boundaries as well and provide it by abundance.

# Free Open Source

Completely free open source MIT licensed code that can be audited and extended by anyone.

# Client side

Hmmtime runs client side. No code is executed serverside. So kinda of Unhosted app.

# Development

Currently Hmmtime is under development for its first Proof of Concept (PoC). Future plans are to decentralise the development by stimulating to fork.

The goal is to develope by W3C and WCAG validated standards and have it secure is possible.

# Organisation

Currently Hmmtime is grassrooted by [Preobroto Sarker](https://gitlab.com/aaryansrk808) as coding developer and with [Jurjen de Vries](https://gitlab.com/jurjendevries) as product owner. Partly used as a study to Web3, decentralized and distributed development. In the future the organization might be transformed into a decentralized autonomous organization (DAO).

# Todo

The todo list of Hmmtime is currently maintained by a public Wekan agile scrum board https://wekan.foss.casa/b/yYzbPe7QKzDWK6MRW/hmmtime

# App documentation

The app is currently in its early stage of development, so no documentation available yet.
